import tkinter as tk
from PIL import ImageTk
from PIL import Image
from PIL import ImageEnhance
import ResizeRatio
import shuffle
import FINALE
from ConcentricRings import alternateRings

def gui_invert_colors(panel):
    new_image = FINALE.imgInvert(panel.photo)

    #update image preview
    panel.photo = new_image
    new_image = ImageTk.PhotoImage(new_image)
    panel.configure(image = new_image)
    panel.image = new_image


def gui_adjust_aspect_ratio(panel):
    """
    Applies the aspect ratio transformation to the image dispayed in the preview panel
    :param panel: the panel containing the image to be modified
    """
    def helper():
        # fetch height and width from text feilds, cnverting from str to int
        width = int (width_input.get())
        height = int (height_input.get())

        # resize image to fit specified aspect ratio
        resized = ResizeRatio.resizeRatio (panel.photo.resize(panel.size), width, height)

        #save ne size
        panel.size = resized.size

        # resize image to fit in preview window
        resized.thumbnail((640, 480))

        # update panel image to resized image
        panel.photo = resized
        new_image = ImageTk.PhotoImage(resized)
        panel.configure(image = new_image)
        panel.image = new_image

        #close popup
        win.destroy()

    # create popup window to get re aspecting ratio
    win = tk.Toplevel()
    win.wm_title("Re-Aspect")

    # create and add textbox input to get width component
    width_input = tk.Entry(win)
    width_input.insert(tk.END, 'width component')
    width_input.grid(row = 0, column = 0)

    # create and add txt item ratio delineator 
    label = tk.Label(win, text = ":")
    label.grid (row = 0, column = 1)

    # create and add textbox input to get height component
    height_input = tk.Entry(win)
    height_input.insert(tk.END, 'height component')
    height_input.grid(row = 0, column = 2)

    # create and add ok button
    b = tk.Button(win, text="Okay", command = helper)
    b.grid(row=1, column=1)


def gui_ring_blend (panel):
    """
    blends the image with a second image in alternative concentric rings
    :param panel: the panel containing the image to be modified
    """
    def helper():
        # open image that will for rings
        to_blend = Image.open(blend_path_input.get())
        
        #blend images together
        blended = alternateRings(panel.photo.resize (panel.size), to_blend)

        # resize blended image to fit preview
        blended.thumbnail((640, 480))

        #update image preview to new blended image
        panel.photo = blended
        new_image = ImageTk.PhotoImage(blended)
        panel.configure(image = new_image)
        panel.image = new_image

        #close popup
        win.destroy()

    # create popup for selecting image to blend
    win = tk.Toplevel()
    win.wm_title("Ring Blend")

    # create and add file path input textbox
    blend_path_input = tk.Entry(win)
    blend_path_input.insert(tk.END, 'path to image')
    blend_path_input.grid(row = 0, column = 0)

    # create and add ok button
    b = tk.Button(win, text="Okay", command = helper)
    b.grid(row=1, column=0)
    

def gui_shuffle (panel):
    """
    apples either shuffle2x2 or shuffle 3x3 to the image
    :param panel: the panel containing the image to be modified
    """
    def helper2():
        # apply effect to image
        new_image = shuffle.shuffle2x2(panel.photo)

        #update image preview
        panel.photo = new_image
        new_image = ImageTk.PhotoImage(new_image)
        panel.configure(image = new_image)
        panel.image = new_image

        #close popup
        win.destroy()
    
    def helper3():
        # apply effect to image
        new_image = shuffle.shuffle3x3(panel.photo)

        #update image preview
        panel.photo = new_image
        new_image = ImageTk.PhotoImage(new_image)
        panel.configure(image = new_image)
        panel.image = new_image

        #close popup
        win.destroy()

    # create popup window to select shuffle type
    win = tk.Toplevel()
    win.wm_title("Shuffle")


    # create and add to window button for electing 2x2 shuffle
    b2 = tk.Button(win, text="2x2", command = helper2)
    b2.grid(row=0, column=0)

    # create and add to window button for electing 3x3 shuffle
    b2 = tk.Button(win, text="3x3", command = helper3)
    b2.grid(row=0, column=1)


def load_image_from_file_path (panel, path):
    """
    Loads an image from a file path into the panel
    :param panel: the panel into which the image will be loaded
    :param path: the path to the image that ill be loaded
    """
    #open image
    im = Image.open(path)
    # record image actual size
    panel.size = im.size
    panel.photo = im

    # resize image to fit in window
    im.thumbnail((640, 480))

    # add image to panel
    img = ImageTk.PhotoImage(im)
    panel.configure(image = img)
    panel.image = img


def save_image(panel, path):
    """
    saves the image from a panel to the specified path
    :param panel: the panel containing the image to be saved
    :param path: the path to which the image will be saved
    """
    # set variable equal to the photo
    to_save = panel.photo.resize(panel.size)
    # save image
    to_save.save(path)
    

def main():
    # create tkinter window
    root = tk.Tk()
    #set window size and resizable to false
    root.geometry("690x665")
    root.resizable(0, 0)
    
    # create image panel
    panel = tk.Label(root)
    # load default image
    load_image_from_file_path (panel, './images/flowers-wall.jpg')

    # add image to window
    panel.grid(columnspan = 10, ipadx = (665 - panel.photo.size[0]), ipady = (505 - panel.photo.size[1]))

    # create and add ring blend button to window
    ring_blend_button = tk.Button(root, width = 15, text="Ring Blend",
        command= lambda : gui_ring_blend (panel))
    ring_blend_button.grid(row = 1, column = 0, ipadx = 5, ipady = 5)

    # create and add aspect ratio adjustment button to window
    aspect_ratio_adjust_button = tk.Button(root, width = 15, text="Adjust aspect ratio",
        command = lambda : gui_adjust_aspect_ratio(panel))
    aspect_ratio_adjust_button.grid(row = 1, column = 1, ipadx = 5, ipady = 5)

    # create and add shuffle button to window
    shuffle_button = tk.Button(root, width = 15, text="Shuffle image",
         command= lambda : gui_shuffle(panel))
    shuffle_button.grid(row = 2, column = 0, ipadx = 5, ipady = 5)

    # create and add color invert button to window
    color_reverse_button = tk.Button(root, width = 15, text="Reverse Colors",
         command= lambda : gui_invert_colors (panel))
    color_reverse_button.grid(row = 2, column = 1, ipadx = 5, ipady = 5)

    # create and add file path input texbox to window
    entry = tk.Entry(root)
    # set initial image path in text box
    entry.insert(tk.END, '/images/flowers-wall.jpg')
    entry.grid(row = 2, column = 4, columnspan = 2, ipadx = 120)

    # create and add save button to window
    save_button = tk.Button(root, text="Save", command= lambda : save_image (panel, entry.get()))
    save_button.grid(row = 1, column = 4, ipadx = 5, ipady = 5)

    # create and add load button to window
    load_button = tk.Button(root, text="Load",
         command = lambda : load_image_from_file_path(panel, entry.get()))
    load_button.grid(row = 1, column = 5, ipadx = 5, ipady = 5)


    root.mainloop()

if __name__ == "__main__":
    main()