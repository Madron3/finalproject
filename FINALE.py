##Armani Rogers
##CSC-355-2
#colorEffects
# Importing imagechops for using the invert() method 
from PIL import Image, ImageChops, ImageEnhance

#Inverts Image color
def imgInvert(img):
    inv_img = ImageChops.invert(img)
    
      
    # Return the output image 
    return inv_img